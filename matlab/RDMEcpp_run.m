function RDMEcpp_run(inputFile, outputFile)
    if ispc
        currentPath = getenv('PATH');
        cmd = ['PATH=' currentPath ' && rdmeCpp-bin\Release\rdmeCpp.exe "' inputFile '" "' outputFile '"'];
    else
        cmd = ['LD_LIBRARY_PATH= && rdmeCpp-bin/rdmeCpp "' inputFile '" "' outputFile '"'];
    end
    disp(cmd);
    system(cmd)
end 