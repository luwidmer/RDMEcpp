function umod = RDMEcpp_compile(umod,varargin)
    p = inputParser;
    addParameter(p, 'verbose', 0);
    addParameter(p, 'solver', 'nsm');
    addParameter(p, 'debug', false);
    parse(p,varargin{:});
    
    solver = lower(p.Results.solver);

    if p.Results.verbose
        %fprintf('urdme_compile(): model_name=%s propensities=%s solvers=%s verbose=%i \n',model_name,propensity_file,solver,verbose);
    end

    RDMEtoolboxRoot = fileparts(mfilename('fullpath'));
    solverCppRoot = [RDMEtoolboxRoot filesep '..' filesep 'cpp' filesep];

    %% if propensity file exists, copy it to ".urdme/model_name.c" (if newer)
%     if ~exist(modelFile, 'file')
%         error('Model file not found.');
%     end
%     
%     javaFileObj = java.io.File(modelFile);
%     if javaFileObj.isAbsolute()
%         absolutePathToModelFile = modelFile;
%     else
%         absolutePathToModelFile = fullfile(pwd, modelFile);
%     end
%     
%     if ispc
%         absolutePathToModelFile = strrep(absolutePathToModelFile, '\', '/');
%     end
%     if ~exist(absolutePathToModelFile, 'file')
%         error('Failed to determine absolute path of model file');
%     end
%     absolutePathToModelFile
%     setenv('RDMEcpp_modelFile', absolutePathToModelFile);
pwd
    setenv('RDMECPP_MODELDIR', pwd);
    %%
    if exist('rdmeCpp-bin/','dir') > 0
        rmdir('rdmeCpp-bin/', 's');
    end

    mkdir('rdmeCpp-bin/');
    
    solverConfig = 'Release';
    if p.Results.debug
        solverConfig = 'Debug';
    end
    
    if ispc
        cmd = ['cmake -G "Visual Studio 14 2015 Win64" "' solverCppRoot '" && cmake --build . --target ALL_BUILD --config ' solverConfig];
    else
        cmd = ['LD_LIBRARY_PATH= && cmake "' solverCppRoot '" && cmake --build . --config ' solverConfig];
    end

    if p.Results.verbose
        fprintf('%s\n',cmd);
    end
    currentPath = cd('rdmeCpp-bin');
    status = system(cmd);
    cd(currentPath);
    
    if status ~= 0
        error('Compilation failed with status %i', status);
    end
%     if r~=0
%         error('%s\nCMD:%s\n',s,cmd);
%     end
end
