function RDMEcpp_save(umod, fileName) %#ok<INUSL>
    warning('off', 'MATLAB:Java:ConvertFromOpaque')
    save(fileName, 'umod', '-v7.3');
    warning('on', 'MATLAB:Java:ConvertFromOpaque')
end