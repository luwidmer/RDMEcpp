function umod = comsol2rdmecpp(fem, verbose)
    %COMSOL2URDME Create an URDME-structure from a COMSOL multiphysics model
    %   UMOD = COMSOL2URDME(FEM,VARARGIN)
    %
    %   --INPUT--
    %   FEM - Comsol Multiphysics 3.5a or 4.x model file
    %   VERBOSE - Silent execution
    %
    %   --OUTPUT--
    %   UMOD - Urdme 1.2 structure with fem-model added to umod.comsol
    %
    %   Generalized subdomains (non-empty argument SDUSED). 
    %   The generalized subdomain vector is given a default value of one. Other 
    %   values can be defined by a special expression (rdme_sd) which should be 
    %   defined on every subdomain, boundary and point. The global expression 
    %   rdme_sdlevel should be defined as the lowest dimension that rdme_sd is 
    %   defined for.
    %

    % L. Widmer    2014-05-16 
    % P. Bauer     2012-08-30
    % V. Gerdin    2012-02-08 (mod2rdme)
    % A. Hellander 2009-11-18 (fem2rdme)
    % S. Engblom   2008-08-01 (fem2rdme)


    if nargin < 2
        verbose = 0;
    end

    % Get extended mesh data
    if verbose > 0
        disp('Starting mphxmeshinfo')
    end
    xmi = mphxmeshinfo(fem); 
    if verbose > 0
        disp('Finished mphxmeshinfo')
    end

    % Get number of species and number of cells.
    [nSpecies, nCells] = size(xmi.nodes.dofs);
    nDoFs = nCells*nSpecies;
    if nDoFs ~= xmi.ndofs
        error('Number of degrees of freedoms mismatch. Check the boundary conditions.');
    end
    
    % permutation of species
    [~,sp] = ismember(xmi.fieldnames,xmi.nodes.dofnames);

    % permutation fem -> rdme
    f2r = reshape(1:nDoFs,nSpecies,nCells);
    f2r = reshape(f2r(sp,:),[ ],1);

    % Create tspan & u0 placeholder, must be redefined in model.m
    tspan = zeros(1, 0);
    u0 = zeros(nSpecies, nCells);

    % Assemble matrices
    if verbose>0
        disp('Start mphmatrix')
    end
    DM = mphmatrix(fem,'sol1','out',{'K','D'});
    if verbose>0
        disp('mphmatrix: Done')
    end
    
    % Check for consistency (compute boundary fluxes option in COMSOL)
    sizes = [size(DM.K) size(DM.D)];
    if any(sizes ~= nDoFs)
        error(['Number of degrees of freedoms mismatch. ' int2str(nDoFs) ' degrees of freedom, matrices have ' int2str(size(DM.K,1)) '. Uncheck the "compute boundary fluxes" option in COMSOL.']);
    end
        
    D = DM.K; % URDME: D "Diffusion matrix"  Comsol: K "Stiffness matrix"
    M = DM.D; % URDME: M "Mass matrix"  Comsol: D "Damping matrix"

    % The (lumped) mass matrix gives the element volume
    vol = full(sum(M,2)); % volume of the dual elements

    % explicitly invert the lumped mass matrix and filter the diffusion matrix
    [i,j,s] = find(D); 

    diagsum1 = sum(diag(D)./vol);
    s = -s./vol(i);
    ixkeep = find(s > 0); % Remove 
    i = i(ixkeep); j = j(ixkeep); s = s(ixkeep);

    % permute and rebuild
    if2r(f2r) = 1:nDoFs;
    [i,j,p] = find(sparse(if2r(i),if2r(j),1:numel(s)));
    D = sparse(i,j,s(p),nDoFs,nDoFs);
    d = full(sum(D,2));
    D = D+sparse(1:nDoFs,1:nDoFs,-d);
    diagsum2 = sum(d);

    % check if the difference is too big
    if abs(diagsum2-diagsum1) > abs(diagsum1)*0.10
        abs(diagsum2-diagsum1)/abs(diagsum1)
        warning('Many off diagonal negative elements in D.');
    end
    % need only volume of unique cells
    vol = vol(f2r);
    vol = vol(1:nSpecies:end);

    % Generalized subdomains. To support more than one subdomain in 
    % the models, the global expression "rdme_sdlevel" has to 
    % be defined.

    sd = nan(nCells, 1);

    % Find rdme_sdlevel value if present
    rdme_sdlvl = -1;
    if verbose>0
        disp('Start mphglobal')
    end
    try
        rdme_sdlvl = mphglobal(fem,'rdme_sdlevel','solnum','end');
    catch
        warning('Global variable "rdme_sdlevel" not set. No subdomains will be used.');
    end
    if verbose>0
        disp('Done')
    end

    if rdme_sdlvl > -1 % i.e. if we found a valid rdme_sdlvl - need to assign subdomains to the different nodes
        % initial sd values, set all to 0
        sd = nan(nCells, 1);

        % sdim - the maximum number of dimensions to run simulation
        sdim = size(xmi.nodes.coords,1);
        % edim2str - translation from numeric edim to string
        
        dimensionOfNodeType = structfun(@comsol2rdmecpp_getDimension, xmi.elements);
        meshTypeNames = fieldnames(xmi.elements);
        [sortedDimensions, dimensionIndex] = sort(dimensionOfNodeType, 'descend');
        
        % The possible mesh types are vtx, edg, quad, tri, quad, tet, hex,
        % prism, and pyr. (COMSOL LiveLink Documentation)
        %edim2str = {'vtx' 'edg' 'tri' 'tet'}; % xmi.elements.meshtypes'; % ?

        % Unit conversion
        unitExpression = ['1[' char(fem.geom('geom1').lengthUnit) '/m]'];
        unitConversion = mphglobal(fem, unitExpression, 'solnum', 1);
        
        for edim = sdim:-1:rdme_sdlvl
            if verbose>0
                fprintf('Processing dimesion %d\n', edim');
            end
            data = mpheval(fem, 'rdme_sd', 'Edim', edim, 'solnum', 1, 'Pattern', 'lagrange');
            data_P = data.p;
            urdme_sd = data.d1;
            
            if size(unique(data.p', 'rows')) ~= size(data.p')
                error('Duplicate Points');
            end
            
            if any(round(urdme_sd(isfinite(urdme_sd))) ~= urdme_sd(isfinite(urdme_sd)))
                error('Zero-dimensional structures not supported');
            end            
            
            % enode_idx - the indices of the nodes involved for a given edim
            nodeTypeIndicesOfCurrentDimension = dimensionIndex(sortedDimensions == edim);
            meshTypeNamesOfCurrentDimension = meshTypeNames(nodeTypeIndicesOfCurrentDimension);
            
            enode_idx = [];
            for currentMeshTypeName = meshTypeNamesOfCurrentDimension'
                currentMeshTypeNameString = currentMeshTypeName{1};
                temp = xmi.elements.(currentMeshTypeNameString).nodes+1;
                temp = temp(:);
                enode_idx = unique([enode_idx; temp]);
            end
            % add 1 to every index as 4.2 indices start from 0

            % coords - x,y,z coordinates of each involved node 
            coords = xmi.nodes.coords(:,enode_idx);

            nSdNodes = size(data_P,2);

            node_idx_perm = zeros(1, nSdNodes);
            for i = 1:nSdNodes
                dist = bsxfun(@minus, data_P(:,i).*unitConversion, coords);
                distScalar = sqrt(sum(dist.^2));

                [foundDist, foundNode] = min(distScalar);

                if foundDist > 10*eps(unitConversion)
                    foundDist
                    urdme_sd(i)
                    warning('Node found is further away than it should be - subdomain cannot be reliably assigned!');
                end
                node_idx_perm(i) = foundNode;
            end

            % Set sd for involved nodes
            sd(enode_idx(node_idx_perm)) = urdme_sd;
        end
        % sd is currently ordered according to comsol nodes. We need to 
        % permute it to be ordered according to comsol dofs (like
        % everything else). 
        sd = sd(xmi.dofs.nodes(1:nSpecies:end)+1);
        
        % add 1 to every index as 4.2 indices start from 0
    end

    % as of the new Comsol API the species' names are refered to as
    % "modelID.speciesName", ex "mod1.MinD_c_atp". The following code
    % removes the new prefix
    tags = fem.modelNode.tags;
    species = xmi.fieldnames;

    for i = 1:size(xmi.fieldnames)
      if strfind(xmi.fieldnames{i},[char(tags(1)) '.']) == 1
          fullName = char(xmi.fieldnames{i});
          species{i} = fullName(size(char(tags(1)),2)+2:end);
      end
    end
    
    
    [ nodeIndexOfDof, ~ ] = RDMEcpp_nodeDofMapping( xmi );

    % Create RDMEcpp structure
    umod = struct();

    umod.tspan    = tspan;
    umod.u0       = u0;
    umod.D        = D';
    umod.vol      = vol;
    umod.sd       = sd;
    umod.nCells   = nCells;
    umod.nSpecies = nSpecies;
    umod.nDoFs    = nDoFs;
    umod.sopts    = [];
    umod.comsol   = fem;
    umod.nodeCoords = xmi.nodes.coords(:, nodeIndexOfDof); % 
end