function dimensionOfElementType = comsol2rdmecpp_getDimension(xmiElementsStruct)
    if (~isstruct(xmiElementsStruct))
        dimensionOfElementType = -1;
        return
    end
    dimensionOfElementType = size(xmiElementsStruct.localcoords,1);
end