function umod = rdmecpp2comsol(umod,U,tspan,verbose)
%URDME2COMSOL Update comsol model in umod.comsol with result matrix U
%   UMOD = URDME2COMSOL(UMOD,U,VERBOSE) Inserts solution matrix U into
%   Umod.comsol
%
%   --INPUT--
%   UMOD - Structure containing URDME simulation parameters and the
%       comsol model in umod.comsol.
%   U - Solution matrix produced by URDME core. U are in absolute integers
%       (number of species), not concentration. URDME2COMSOL turns values to 
%       consentration before saving to the model object. 
%   tspan - Times span vector that matches the size of U. Defaults to umod.tspan.  
%   VERBOSE - Silent execution
%
%   --OUTPUT--
%   UMOD - Same as input but with U added to Solution 'sol1' of umod.cosmol
%
%   See also COMSOL2URDME, URDME.

% L. Widmer     2014-05-16
% P. Bauer      2012-08-30
% V. Gerdin     2012-02-27 (rdme2mod)
% A. Hellander  2010-05-04 (rdme2fem)
% J. Cullhed    2008-08-06 (rdme2fem)

if nargin < 3
    tspan = umod.tspan;
end

if nargin < 4
    verbose = 0;
end
  


%extend mesh from model
xmi = mphxmeshinfo(umod.comsol);

% RESHAPE U TO FIT MODEL

% fundamentals
nodes = xmi.nodes;
[Mspecies,Ncells] = size(nodes.dofs);
Ndofs = Mspecies*Ncells;

% permutation of species
if isfield(umod,'species')
  fieldnames = umod.species;
else
  fieldnames = xmi.fieldnames;
end

% Fixed for Comsol 4.3
modName = char(umod.comsol.modelNode.tags());
if isempty(strfind(char(fieldnames(1)),modName))
    fieldnames=strcat(modName,'.',fieldnames);
end
[~,sp] = ismember(fieldnames,nodes.dofnames);
isp(sp) = 1:numel(sp); %fix order

% Scale U
U = reshape(U,Mspecies,Ncells,[]);

% Change from absolute figures to concentration
% suboptimal, but should work ok
vol = umod.vol;



% Compute concentrations
U = bsxfun(@rdivide, U, vol(:)'.*6.022e23 );


%%

% Finally reshape U in order of Dofs
U = reshape(U(isp,:,:),Ndofs,[]);

% Store U in model
if verbose>0
    disp('Saving U to Model')
end
umod.comsol.disableUpdates(true);
if verbose > 1
    waitbarHandle = waitbar(0, 'Importing solution into COMSOL...', 'Name', 'Importing solution into COMSOL...', 'CreateCancelBtn', 'setappdata(gcbf,''canceling'',1)');
    setappdata(waitbarHandle, 'canceling', 0)
end
nTimeSteps = length(tspan);
for i=1:nTimeSteps
    umod.comsol.sol('sol1').setU(i, U(:,i)); % [ones(szOrig(1) - Ndofs,1);
    if verbose > 1
        if getappdata(waitbarHandle,'canceling')
            break
        end
        if mod(i,10) == 1
            waitbar(i/nTimeSteps,waitbarHandle,sprintf('%3.1f%%',100*i/nTimeSteps))
        end
    end
end
if verbose > 1
    waitbar(1,waitbarHandle,'Updating Model in COMSOL');
end
umod.comsol.sol('sol1').setPVals(tspan);
umod.comsol.sol('sol1').createSolution;
umod.comsol.disableUpdates(false);
if verbose > 1
    delete(waitbarHandle);
end



if verbose>0
	disp('Solution import finished');
end