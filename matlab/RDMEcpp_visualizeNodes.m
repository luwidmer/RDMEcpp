function RDMEcpp_visualizeNodes(umod, varargin)
    p = inputParser;
    addParameter(p, 'createFigure', true, @(x) all(islogical(x) & isscalar(x)));
    addParameter(p, 'axisEqual', true, @(x) all(islogical(x) & isscalar(x)));
    parse(p,varargin{:});

    if p.Results.createFigure
        figure
    end
    
    dimension = size(umod.nodeCoords, 1);
    
    if dimension == 3
        if all(isnan(umod.sd))
            scatter3(umod.nodeCoords(1, :), umod.nodeCoords(2, :), umod.nodeCoords(3, :), 5, [0 0 0], 'filled');
        else
            scatter3(umod.nodeCoords(1, :), umod.nodeCoords(2, :), umod.nodeCoords(3, :), 5, umod.sd, 'filled');
            if any(isnan(umod.sd))
                hold on;
                nanNodes = isnan(umod.sd);
                h = scatter3(umod.nodeCoords(1, nanNodes), umod.nodeCoords(2, nanNodes), umod.nodeCoords(3, nanNodes), 100, [1 0 0], 'filled');
                legend(h, 'Nodes with unassigned domain', 'Location', 'northoutside');
                legend boxoff
                warning('Not all nodes have subdomains assigned in this model (any(isnan(umod.sd)))!');
            end
        end
        xlabel('x (m)');
        ylabel('y (m)');
        zlabel('z (m)');
    elseif dimension == 2
        scatter(umod.nodeCoords(1, :), umod.nodeCoords(2, :), 5, umod.sd, 'filled');
    else
        error('Not implemented (yet)');
    end
    
    if dimension >= 2
        if p.Results.axisEqual
            axis equal
        end
    end
    
    if ~all(isnan(umod.sd))
        c = colorbar;
        c.Ticks = unique(umod.sd);
        c.Label.String = 'Subdomains';
    else
        warning(sprintf('No subdomains were detected in this model (all(isnan(umod.sd)))!\nIf the model does not have subdomains, it is safe to ignore this warning.')); %#ok<SPWRN>
    end
end