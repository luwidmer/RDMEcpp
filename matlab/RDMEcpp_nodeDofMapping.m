function [ nodeIndexOfDof, dofIndexOfNode ] = RDMEcpp_nodeDofMapping( xmi )
    nSpecies = length(xmi.nodes.dofnames);
    
    nodeIndexOfDof = xmi.dofs.nodes(1:nSpecies:end)+1; % gives the node index for each DoF
    [~, dofIndexOfNode] = sort(nodeIndexOfDof);        % gives the DoF  index for each node
end

