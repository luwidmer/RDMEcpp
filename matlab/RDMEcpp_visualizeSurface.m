function RDMEcpp_visualizeSurface(umod, speciesIndex, varargin)
    p = inputParser;
    addParameter(p, 'surfaceTriangles', []);
    addParameter(p, 'movieFile', '');
    addParameter(p, 'framerate', 10);
    addParameter(p, 'movieQuality', 75);
    addParameter(p, 'movieProfile', 'Motion JPEG AVI');
    addParameter(p, 'subdomain', nan);
    addParameter(p, 'timeIndices', 1:length(umod.tspan));
    addParameter(p, 'axisOptions', {'image'});
    addParameter(p, 'colorMap', []);
    addParameter(p, 'trisurfOptions', {'EdgeAlpha', 0.2, 'EdgeColor', [0 0 0],'FaceAlpha', 1, 'LineWidth', 0.32, 'FaceColor', 'interp'});
    
    parse(p,varargin{:});

    %% Scan solver output first to define color axis
    concentration = umod.U(speciesIndex:umod.nSpecies:end, :) ./ repmat(umod.vol,1,size(umod.U,2));
    
    %% By default, plot the surface of the model
    if isempty(p.Results.surfaceTriangles)
        [ ~, dofIndexOfNode ] = RDMEcpp_nodeDofMapping( umod.xmi );
        surfaceTriangles = dofIndexOfNode(1 + umod.xmi.elements.tri.nodes');
    else
        surfaceTriangles = p.Results.surfaceTriangles;
    end
    
    
    %% Plot
    if ~isnan(p.Results.subdomain)
        domain = umod.sd == p.Results.subdomain;
        concentration = concentration(domain, :);
    end
    
    X = umod.nodeCoords(1,:);
    Y = umod.nodeCoords(2,:);
    Z = umod.nodeCoords(3,:);

    %%    
    if ~isempty(p.Results.axisOptions)
    	axis(p.Results.axisOptions{:});
    end
    
    view(0, 0);
    box off;
    xlabel('x (m)');
    ylabel('y (m)');
    zlabel('z (m)');
    
    maxValue = quantile(concentration(:), 0.99);
    caxis([0 maxValue])
    if ~isempty(p.Results.colorMap)
        colormap(p.Results.colorMap);
    end
    hold on;
    
    h = 0;
    if ~isempty(p.Results.movieFile)
        myVideo = VideoWriter(p.Results.movieFile, p.Results.movieProfile);
        myVideo.FrameRate = p.Results.framerate;
        myVideo.Quality = p.Results.movieQuality;
        open(myVideo);
    end
    for i = p.Results.timeIndices
        if h ~= 0
            delete(h); 
        end
        tStart = tic;
        h = trisurf(surfaceTriangles,X,Y,Z,concentration(:,i), p.Results.trisurfOptions{:});
        title(sprintf('Time = %g seconds\n', umod.tspan(i)));
        drawnow
        if ~isempty(p.Results.movieFile)
            writeVideo(myVideo, getframe(gcf));
        end
        tEnd = toc(tStart);
        
        if isempty(p.Results.movieFile)
            % Only wait if we are not exporting a video
            timeToWait = max(0, 1/p.Results.framerate - tEnd);
            if timeToWait > 0
                pause(timeToWait);
            end
        end
    end
    if ~isempty(p.Results.movieFile)
        close(myVideo);
    end
end