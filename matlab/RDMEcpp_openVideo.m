function RDMEcpp_openVideo(fileName)
    if ispc
        winopen(fileName);
    elseif ismac
        system(['open ' fileName ' &']);
    else
        system(['xdg-open ' fileName ' &']);
    end
end