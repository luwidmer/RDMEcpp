function umod = RDMEcpp_load(umod, outputFile) 
    output = load(outputFile);
    names = fieldnames(output.umod)';
    for fieldName = names
        if strcmp(fieldName{1}, 'comsol')
            continue;
        end
        umod.(fieldName{1}) = output.umod.(fieldName{1});
    end
end