#include "NextSubvolumeMethod.h"
#include <queue>
#include <functional>
#include <iostream> 
#include <iomanip>


NextSubvolumeMethod::NextSubvolumeMethod(Model &model) : model(model)
{
	// Allocate memory for required datastructures
	subvolumeReactionRates.resize(model.nReactions, model.nCells);
	totalSubvolumeReactionRates.resize(model.nCells);
	totalSubvolumeDiffusionRates.resize(model.nCells);
	totalSubvolumeRates.resize(model.nCells);
	diffusionMatrixDiagonal.resize(model.nCells*model.nSpecies);
	currentState.resize(model.nSpecies, model.nCells);

	totalReactionEvents = 0;
	totalDiffusionEvents = 0;
}


NextSubvolumeMethod::~NextSubvolumeMethod()
{
}

void NextSubvolumeMethod::Run()
{
	currentState = model.intialCondition;

	/* If model has seed, apply it */
	if (model.hasSeed)
	{
		unifRng.seed((uint32_t)model.seed);
	}

	/* Initialize propensities for all cells */
	for (decltype(model.nCells) cellIndex = 0; cellIndex < model.nCells; cellIndex++)
	{
		/* Initialize propensities for all reactions in the current cell */
		for (decltype(model.nReactions) reactionIndex = 0; reactionIndex < model.nReactions; reactionIndex++)
		{ // VectorXd& species, VectorXd& data, double& volume, int& subDomain
			subvolumeReactionRates(reactionIndex, cellIndex) =
				model.propensityFunctions[reactionIndex](currentState.col(cellIndex), cellIndex);
		}
	}

	/* Calculate the total reaction propensity for each cell */
	totalSubvolumeReactionRates = subvolumeReactionRates.colwise().sum();
	
	/* Cache the negative diffusion matrix diagonal as a dense vector */
	diffusionMatrixDiagonal = -model.diffusionMatrix.diagonal();

	/* Calculate the total diffusion propensity for each cell */
	totalSubvolumeDiffusionRates.setZero();
	for (decltype(model.nCells) cellIndex = 0; cellIndex < model.nCells; cellIndex++)
	{
		for (decltype(model.nSpecies) speciesIndex = 0; speciesIndex < model.nSpecies; speciesIndex++)
		{
			auto index = cellIndex*model.nSpecies + speciesIndex;
			totalSubvolumeDiffusionRates(cellIndex) += diffusionMatrixDiagonal(index) * currentState(speciesIndex, cellIndex);
			// cout << cellIndex << " : " << speciesIndex << " : " << diffusionMatrixDiagonal(index) << " : " << currentState(speciesIndex, cellIndex) << " : " << totalSubvolumeDiffusionRates(cellIndex) << endl;
		}
		
	}

	vector<tuple<double, int>> reactionRateTimeVector;
	vector<double> reactionRateVector;
	reactionRateTimeVector.reserve(model.nCells);
	reactionRateVector.reserve(model.nCells);

	for (decltype(model.nCells) cellIndex = 0; cellIndex < model.nCells; cellIndex++)
	{
		totalSubvolumeRates(cellIndex) = totalSubvolumeDiffusionRates(cellIndex) + totalSubvolumeReactionRates(cellIndex);
		if (totalSubvolumeRates(cellIndex) > 0.0)
		{
			// reactionRateTimeVector.emplace_back(-log(1.0 - dist(rng)) / totalSubvolumeRates(cellIndex) + model.sampleTimePoints(0), cellIndex);
			//reactionRateTimeVector.emplace_back(expDist(unifRng, totalSubvolumeRates(cellIndex)) + model.sampleTimePoints(0), cellIndex);
			reactionRateVector.emplace_back(expDist(unifRng, totalSubvolumeRates(cellIndex)) + model.sampleTimePoints(0));
		}
		else
		{
			// reactionRateTimeVector.emplace_back(numeric_limits<double>::infinity(), cellIndex);
			reactionRateVector.emplace_back(numeric_limits<double>::infinity());
		}
	}
	// fastHeap.make_heap(reactionRateTimeVector.begin(), reactionRateTimeVector.end());
	// reactionRateTimes = BinHeap<int, double>(reactionRateTimeVector);
	// tournamentTree = TournamentTree::TournamentTree<double, short>(reactionRateVector);
	tournamentTreePointer = TournamentTree::TournamentTreePointer(reactionRateVector);

	

	auto endTime = model.sampleTimePoints.bottomRows(1).value();
	auto currentEvent = tournamentTreePointer.GetMinimum();
#ifndef WIN32
	while (__builtin_expect((currentTime = get<1>(currentEvent)) < endTime,1))
#else
	while ((currentTime = get<1>(currentEvent)) < endTime)
#endif
	{
		/*std::cout << "BinHeap Subvolume: " << get<1>(reactionRateTimes.data_[0]) << ", TT Subvolume: " << get<0>(tournamentTree.GetMinimum()) << std::endl;
		std::cout << "BinHeap Timme    : " << get<0>(reactionRateTimes.data_[0]) << ", TT Time     : " << get<1>(tournamentTree.GetMinimum()) << std::endl; 
		
		if (get<1>(reactionRateTimes.data_[0]) != get<0>(tournamentTree.GetMinimum()))
		{
			if (get<0>(reactionRateTimes.data_[0]) != get<1>(tournamentTree.GetMinimum()))
			{
				std::cout << "BinHeap Subvolume: " << get<1>(reactionRateTimes.data_[0]) << ", TT Subvolume: " << get<0>(tournamentTree.GetMinimum()) << std::endl;
				std::cout << "BinHeap Time     : " << get<0>(reactionRateTimes.data_[0]) << ", TT Time     : " << get<1>(tournamentTree.GetMinimum()) << std::endl;

				throw runtime_error("Discrepancy found!");
			}
			
		}


		std::cout << "BinHeap Subvolume: " << get<1>(reactionRateTimes.data_[0]) << ", TT Subvolume: " << get<0>(tournamentTreePointer.GetMinimum()) << std::endl;
		std::cout << "BinHeap Timme    : " << get<0>(reactionRateTimes.data_[0]) << ", TT Time     : " << get<1>(tournamentTreePointer.GetMinimum()) << std::endl; 

		if (get<1>(reactionRateTimes.data_[0]) != get<0>(tournamentTreePointer.GetMinimum()))
		{
			if (get<0>(reactionRateTimes.data_[0]) != get<1>(tournamentTreePointer.GetMinimum()))
			{

				std::cout << "BinHeap Subvolume: " << get<1>(reactionRateTimes.data_[0]) << ", TTP Subvolume: " << get<0>(tournamentTreePointer.GetMinimum()) << std::endl;
				std::cout << "BinHeap Time     : " << get<0>(reactionRateTimes.data_[0]) << ", TTP Time     : " << get<1>(tournamentTreePointer.GetMinimum()) << std::endl;
				throw runtime_error("Discrepancy found!");
			}
		}
		*/
		model.SaveResultFunction(currentTime, currentState);

		auto nextReactionVolumeIndex = get<0>(currentEvent);

		auto randEvent = unifDist(unifRng) * totalSubvolumeRates(nextReactionVolumeIndex);
		auto diffusionRateForTargetSubvolume = totalSubvolumeDiffusionRates(nextReactionVolumeIndex);

		if (randEvent < diffusionRateForTargetSubvolume)
		{
			// Diffusion event
			auto diffusionTargetVolumeIndex = PerformDiffusionEvent(randEvent, nextReactionVolumeIndex);

			// Generate new event for the other subvolume
			// auto targetVolume = reactionRateTimes.positionInHeap_[diffusionTargetVolumeIndex];
			if (totalSubvolumeRates(diffusionTargetVolumeIndex) > 0.0)
			{
				//get<0>(reactionRateTimes.data_[targetVolume]) = -log(1.0 - dist(rng)) / totalSubvolumeRates(diffusionTargetVolumeIndex) + currentTime;
				// get<0>(reactionRateTimes.data_[targetVolume]) = expDist(unifRng, totalSubvolumeRates(diffusionTargetVolumeIndex)) + currentTime;
				tournamentTreePointer.UpdateLeaf(diffusionTargetVolumeIndex, expDist(unifRng, totalSubvolumeRates(diffusionTargetVolumeIndex)) + currentTime);
			}
			else
			{
				tournamentTreePointer.UpdateLeaf(diffusionTargetVolumeIndex, numeric_limits<double>::infinity());

				//get<0>(reactionRateTimes.data_[targetVolume]) = numeric_limits<double>::infinity();
			}
			// tournamentTree.UpdateLeaf(diffusionTargetVolumeIndex, get<0>(reactionRateTimes.data_[targetVolume]));
			// tournamentTreePointer.UpdateLeaf(diffusionTargetVolumeIndex, get<0>(reactionRateTimes.data_[targetVolume]));
			// reactionRateTimes.Update(targetVolume);
		}
		else
		{
			// Reaction event
			PerformReactionEvent(randEvent - diffusionRateForTargetSubvolume, nextReactionVolumeIndex);
		}
		
		// Generate new event for the current subvolume	
		if (totalSubvolumeRates(nextReactionVolumeIndex) > 0.0)
		{
			// get<0>(reactionRateTimes.data_[0]) = -log(1.0 - dist(rng)) / totalSubvolumeRates(nextReactionVolumeIndex) + currentTime;
			// get<0>(reactionRateTimes.data_[0]) = expDist(unifRng, totalSubvolumeRates(nextReactionVolumeIndex)) + currentTime;
			tournamentTreePointer.UpdateLeaf(nextReactionVolumeIndex, expDist(unifRng, totalSubvolumeRates(nextReactionVolumeIndex)) + currentTime);
		}
		else
		{
			// get<0>(reactionRateTimes.data_[0]) = numeric_limits<double>::infinity();
			tournamentTreePointer.UpdateLeaf(nextReactionVolumeIndex, numeric_limits<double>::infinity());

		}
		// tournamentTree.UpdateLeaf(nextReactionVolumeIndex, get<0>(reactionRateTimes.data_[0]));
		// tournamentTreePointer.UpdateLeaf(nextReactionVolumeIndex, get<0>(reactionRateTimes.data_[0]));

		// reactionRateTimes.Update(0); // Percolate the root node
		currentEvent = tournamentTreePointer.GetMinimum();
	}
	model.SaveResultFunction(currentTime, currentState);

	cout << "Reaction Events: " << totalReactionEvents << endl;
	cout << "Diffusion Events: " << totalDiffusionEvents << endl;
}

void NextSubvolumeMethod::PerformReactionEvent(double randEvent, int nextReactionVolumeIndex)
{
	// Determine the reaction that occurs (direct SSA)
	decltype(randEvent) currentCumulativeRate = 0;
	decltype(model.nReactions) reactionEvent = 0;

	decltype(model.nReactions) lastReaction = model.nReactions - 1;

	if (randEvent == 0.0)
	{
		cout << setprecision(16) << randEvent << endl;
		throw runtime_error("Reaction event with probability zero should not execute, exiting!");
	}

	while (true)
	{
		currentCumulativeRate += subvolumeReactionRates(reactionEvent, nextReactionVolumeIndex); // diffusionMatrixDiagonal(diffusingSpecies + startDegreeOfFreedom)*currentState(diffusingSpecies, nextReactionVolumeIndex);

		if (currentCumulativeRate >= randEvent || reactionEvent >= lastReaction)
		{
			break;
		}

		reactionEvent++;
	}
	

	// Execute reaction in bulk & Update diffusion propensities for subvolume
	for (SparseMatrix<int>::InnerIterator it(model.stoichiometricMatrix, reactionEvent); it; ++it)
	{
		currentState(it.row(), nextReactionVolumeIndex) += it.value();
	
#ifdef _DEBUG 
		// Sanity check after executing reaction
		if (currentState(it.row(),nextReactionVolumeIndex) < 0)
		{
			throw runtime_error("Reaction event caused negative concentration, exiting!");
		}
#endif	

		auto index = nextReactionVolumeIndex*model.nSpecies + it.row();
		totalSubvolumeDiffusionRates(nextReactionVolumeIndex) += diffusionMatrixDiagonal(index)*it.value();
	}

	// Tell model to execute reaction
	if (model.reactionFunctions.size())
	{
		model.reactionFunctions[reactionEvent](currentTime, currentState.col(nextReactionVolumeIndex), nextReactionVolumeIndex);
	}


	// Update reaction propensities according to dependency graph
	for(SparseMatrix<int>::InnerIterator it(model.dependencyGraphReaction, reactionEvent); it; ++it)
	{
		auto reactionToUpdate = it.row();
				
		auto newReactionRate = model.propensityFunctions[reactionToUpdate](currentState.col(nextReactionVolumeIndex), nextReactionVolumeIndex);
		auto reactionRateDelta = newReactionRate - subvolumeReactionRates(reactionToUpdate, nextReactionVolumeIndex);

		subvolumeReactionRates(reactionToUpdate, nextReactionVolumeIndex) = newReactionRate;
		totalSubvolumeReactionRates(nextReactionVolumeIndex) += reactionRateDelta;
	}

	// Update total propensities
	totalSubvolumeRates(nextReactionVolumeIndex) = totalSubvolumeDiffusionRates(nextReactionVolumeIndex) + totalSubvolumeReactionRates(nextReactionVolumeIndex);

	totalReactionEvents++;
}


int NextSubvolumeMethod::PerformDiffusionEvent(double randEvent, int nextReactionVolumeIndex)
{
	/*
	cout << "Diffusion event for volume index " << nextReactionVolumeIndex << endl;
	cout << "---------------------------------" << endl;
	cout << "Random Rate: " << randEvent << endl;
	cout << "Max. Diffusion Rate for Volume: " << totalSubvolumeDiffusionRates(nextReactionVolumeIndex) << endl;
	cout << "Max. Rate for Volume: " << totalSubvolumeRates(nextReactionVolumeIndex) << endl;
	*/
	// Determine which species diffuses	

	decltype(nextReactionVolumeIndex) startDegreeOfFreedom = nextReactionVolumeIndex*model.nSpecies;

	decltype(nextReactionVolumeIndex) diffusingSpecies = 0;
	decltype(nextReactionVolumeIndex) endSpecies = model.nSpecies - 1;
	
	decltype(randEvent) currentCumulativeRate = 0;
	
	while (true)
	{
		currentCumulativeRate += diffusionMatrixDiagonal(diffusingSpecies + startDegreeOfFreedom)*currentState(diffusingSpecies, nextReactionVolumeIndex);

		if (currentCumulativeRate >= randEvent || diffusingSpecies >= endSpecies)
		{
			break;
		}

		diffusingSpecies++;
	} 
	
	decltype(nextReactionVolumeIndex) currentDegreeOfFreedom = startDegreeOfFreedom + diffusingSpecies;

	// Determine other participating subvolume
	decltype(randEvent) randDirection = unifDist(unifRng)*diffusionMatrixDiagonal(currentDegreeOfFreedom);
	decltype(nextReactionVolumeIndex) diffusionTargetDegreeOfFreedom = -1;

	currentCumulativeRate = 0;

	for (SparseMatrix<double>::InnerIterator it(model.diffusionMatrix, currentDegreeOfFreedom); it; ++it)
	{
		auto currentDiffusionEvent = it.row();
		if (currentDiffusionEvent == currentDegreeOfFreedom)
			continue;
		
		currentCumulativeRate += it.value();
		diffusionTargetDegreeOfFreedom = currentDiffusionEvent;
		if (currentCumulativeRate >= randDirection)
		{
			break;
		}
	}
	
#ifdef _DEBUG
	if (diffusionTargetDegreeOfFreedom == currentDegreeOfFreedom)
	{
		throw runtime_error("Diffusion target the same!");
	}
	

	// cout << "Diffusing Species: " << diffusingSpecies << ", Target Species: " << (diffusionTargetDegreeOfFreedom % model.nSpecies) << endl;
	
	if (diffusingSpecies != (diffusionTargetDegreeOfFreedom % model.nSpecies))
	{
		throw runtime_error("Diffusion target wrong!");
	}
	
	if (diffusionTargetDegreeOfFreedom < 0)
	{
		throw runtime_error("Diffusion target not found, exiting!");
	}
#endif

	decltype(nextReactionVolumeIndex) diffusionTargetSubvolume = diffusionTargetDegreeOfFreedom/model.nSpecies;

	// Execute diffusion event
	currentState(diffusingSpecies, nextReactionVolumeIndex)--;
	currentState(diffusingSpecies, diffusionTargetSubvolume)++;

#ifdef _DEBUG
	if (currentState(diffusingSpecies, nextReactionVolumeIndex) < 0)
	{
		cout << "Diffusion event caused negative concentration, exiting!" << endl;
		throw runtime_error("Diffusion event caused negative concentration, exiting!");
	}
#endif

	

	// Update diffusion propensities
	totalSubvolumeDiffusionRates(nextReactionVolumeIndex) -= diffusionMatrixDiagonal(currentDegreeOfFreedom);
	totalSubvolumeDiffusionRates(diffusionTargetSubvolume) += diffusionMatrixDiagonal(diffusionTargetDegreeOfFreedom);

	// Update reaction propensities according to dependency graph

	double totalReactionRateDiffusionSource = 0;
	double totalReactionRateDiffusionTarget = 0;
	for(SparseMatrix<int>::InnerIterator it(model.dependencyGraphDiffusion, diffusingSpecies); it; ++it)
	{
		auto reactionToUpdate = it.row();

		auto newReactionRateFrom = model.propensityFunctions[reactionToUpdate](currentState.col(nextReactionVolumeIndex), nextReactionVolumeIndex);
		auto newReactionRateTo = model.propensityFunctions[reactionToUpdate](currentState.col(diffusionTargetSubvolume), diffusionTargetSubvolume);

		auto reactionRateDeltaFrom = newReactionRateFrom - subvolumeReactionRates(reactionToUpdate, nextReactionVolumeIndex);
		auto reactionRateDeltaTo = newReactionRateTo - subvolumeReactionRates(reactionToUpdate, diffusionTargetSubvolume);

		subvolumeReactionRates(reactionToUpdate, nextReactionVolumeIndex) = newReactionRateFrom;
		subvolumeReactionRates(reactionToUpdate, diffusionTargetSubvolume) = newReactionRateTo;

		totalReactionRateDiffusionSource += reactionRateDeltaFrom;
		totalReactionRateDiffusionTarget += reactionRateDeltaTo;
	}

	totalSubvolumeReactionRates(nextReactionVolumeIndex) += totalReactionRateDiffusionSource;
	totalSubvolumeReactionRates(diffusionTargetSubvolume) += totalReactionRateDiffusionTarget;

	totalSubvolumeRates(nextReactionVolumeIndex) += totalReactionRateDiffusionSource - diffusionMatrixDiagonal(currentDegreeOfFreedom);
	totalSubvolumeRates(diffusionTargetSubvolume) += totalReactionRateDiffusionTarget + diffusionMatrixDiagonal(diffusionTargetDegreeOfFreedom);


	/*
	cout << "Reaction Rate of Source Volume: " << totalSubvolumeReactionRates(nextReactionVolumeIndex) << endl;
	cout << "Reaction Rate of Source Volume (sum): " << subvolumeReactionRates.col(nextReactionVolumeIndex).sum() << endl;
	cout << "Total Rate of Source Volume: " << totalSubvolumeRates(nextReactionVolumeIndex) << endl;
	cout << "Total Rate of Source Volume (sum): " << totalSubvolumeReactionRates(nextReactionVolumeIndex) + totalSubvolumeDiffusionRates(nextReactionVolumeIndex) << endl;

	cout << "Reaction Rate of Target Volume: " << totalSubvolumeReactionRates(diffusionTargetSubvolume) << endl;
	cout << "Reaction Rate of Target Volume (sum): " << subvolumeReactionRates.col(diffusionTargetSubvolume).sum() << endl;
	cout << "Total Rate of Target Volume: " << totalSubvolumeRates(diffusionTargetSubvolume) << endl;
	cout << "Total Rate of Target Volume (sum): " << totalSubvolumeReactionRates(diffusionTargetSubvolume) + totalSubvolumeDiffusionRates(diffusionTargetSubvolume) << endl;
	*/

	
	

	totalDiffusionEvents++;

	return diffusionTargetSubvolume;
}

void NextSubvolumeMethod::UpdateSubvolumePropensities(int subVolumeIndex)
{
	// Update Diffusion Propensities
	totalSubvolumeDiffusionRates(subVolumeIndex) = 0;
	for (decltype(model.nSpecies) speciesIndex = 0; speciesIndex < model.nSpecies; speciesIndex++)
	{
		auto index = subVolumeIndex*model.nSpecies + speciesIndex;
		totalSubvolumeDiffusionRates(subVolumeIndex) += diffusionMatrixDiagonal(index) * currentState(speciesIndex, subVolumeIndex);
	}

	// Update Reaction Propensities

	/* Initialize propensities for all reactions in the current cell */
	for (decltype(model.nReactions) reactionIndex = 0; reactionIndex < model.nReactions; reactionIndex++)
	{ // VectorXd& species, VectorXd& data, double& volume, int& subDomain
		subvolumeReactionRates(reactionIndex, subVolumeIndex) =
			model.propensityFunctions[reactionIndex](currentState.col(subVolumeIndex), subVolumeIndex);
	}

	/* Calculate the total reaction propensity for each cell */
	totalSubvolumeReactionRates(subVolumeIndex) = subvolumeReactionRates.col(subVolumeIndex).sum();

	totalSubvolumeRates(subVolumeIndex) = totalSubvolumeDiffusionRates(subVolumeIndex) + totalSubvolumeReactionRates(subVolumeIndex);

	// Update Sum
	/*auto targetVolume = reactionRateTimes.positionInHeap_[subVolumeIndex];
	if (totalSubvolumeRates(subVolumeIndex) > 0.0)
	{
		get<0>(reactionRateTimes.data_[targetVolume]) = -log(1.0 - unifDist(unifRng)) / totalSubvolumeRates(subVolumeIndex) + currentTime;
	}
	else
	{
		get<0>(reactionRateTimes.data_[targetVolume]) = numeric_limits<double>::infinity();
	}
	reactionRateTimes.Update(targetVolume);*/

}
