#pragma once
#include <Eigen/Dense>
#include <functional>
#include <vector>

using namespace std;
using namespace Eigen;

typedef const DenseBase<MatrixXd>::ColXpr & VectorType;

class Propensities
{
public:

	 Propensities(void);
	~Propensities(void);



	vector<function<double(VectorType, VectorType, const double&, const int&)>> propensityFunctions; // TO DO: Get Propensity Functions

	const int cGDPTubulin = 0;
	const int cGTPTubulin = 1;
	const int cGTPTubulin_MT = 2;
	const int cGDPTubulin_MT = 3;

	const int CYTOSOL = 1;
	const int MICROTUBULE = 2;

	const int NR = 5;

	/* Rate constants. */
	const double NA = 6.022e23;
	const double kd = 1.25e-8;
	const double kdd = 9.0e6;
	const double kde = 5.56e7;
	const double ke = 0.7;
	const double k_adp = 1.0;
};