#include "Propensities.h"

Propensities::Propensities()
{
	propensityFunctions.reserve(5);
	propensityFunctions.push_back(std::bind(&Propensities::rFun1, this, placeholders::_1, placeholders::_2, placeholders::_3, placeholders::_4));
	propensityFunctions.push_back(std::bind(&Propensities::rFun2, this, placeholders::_1, placeholders::_2, placeholders::_3, placeholders::_4));
	propensityFunctions.push_back(std::bind(&Propensities::rFun3, this, placeholders::_1, placeholders::_2, placeholders::_3, placeholders::_4));
	propensityFunctions.push_back(std::bind(&Propensities::rFun4, this, placeholders::_1, placeholders::_2, placeholders::_3, placeholders::_4));
	propensityFunctions.push_back(std::bind(&Propensities::rFun5, this, placeholders::_1, placeholders::_2, placeholders::_3, placeholders::_4));
}

Propensities::~Propensities()
{

}
/*
% 0: 
% 1 : cGTPTubulin->cGTPTubulin_MT(MT Growth)
% 2 : cGTPTubulin_MT->cGDPTubulin_MT(MT GTP Hydrolysis)
% 3 : cGDPTubulin_MT->cGDPTubulin(MT Shrinkage)
% 4 : cGTPTubulin->cGDPTubulin(GTP Loss)
*/

