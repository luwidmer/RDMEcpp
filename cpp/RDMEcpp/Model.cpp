#include "Model.h"
#include <iostream>
#include <iomanip>

Model::Model(const MatlabFile &file)
{
	long long nCells = (long long int)file.GetValue("nCells");
	cout << "# Cells: " << nCells << endl;
	this->nCells = nCells;

	sampleTimePoints = file.GetVector("tspan");
	subDomains = file.GetVector("sd").cast<int>();
	
	customData = file.GetMatrix("data");

	intialCondition = file.GetMatrix("u0").cast<int>();

	try
	{
		seed = file.GetValue("seed");
		hasSeed = true;
	}
	catch (H5::Exception& e)
	{
		hasSeed = false;
		cout << "h5 exception: "  << endl;
	}
	catch (exception& e)
	{
		hasSeed = false;
		cout << "Standard exception: " << e.what() << endl;
	}

	stoichiometricMatrix = file.GetSparseMatrix("N").cast<int>();
	cout << stoichiometricMatrix;
	dependencyGraphDiffusion = file.GetSparseMatrix("Gdiffusion").cast<int>();
	dependencyGraphReaction = file.GetSparseMatrix("Greaction").cast<int>();
	diffusionMatrix = file.GetSparseMatrix("D");

	nReactions = stoichiometricMatrix.rows(); // TO DO: Check for consistency, implement const getters
	nSpecies = stoichiometricMatrix.cols();

	solution = MatrixXi(nSpecies*nCells, sampleTimePoints.size()); // file.GetMatrix("U");

	volumes = file.GetVector("vol");

	sampleTimePointCount = sampleTimePoints.size();
	nextSampleTime = sampleTimePoints(0);
	endTime = sampleTimePoints.bottomRows(1).value();
}

void Model::Save(MatlabFile& file)
{
	file.SetValue("Ncells", nCells);
	file.SetVector("tspan", sampleTimePoints);
	file.SetVector("sd", subDomains.cast<double>());
	file.SetMatrix("data", customData);
	file.SetMatrix("U", solution.cast<double>());
	file.SetMatrix("u0", intialCondition.cast<double>());
	
	if (hasSeed)
	{
		file.SetValue("seed", seed);
	}

	file.SetSparseMatrix("N", stoichiometricMatrix.cast<double>());
	file.SetSparseMatrix("Gdiffusion", dependencyGraphDiffusion.cast<double>());
	file.SetSparseMatrix("Greaction", dependencyGraphReaction.cast<double>());
	file.SetSparseMatrix("D", diffusionMatrix);

	file.SetVector("vol", volumes);
}

Model::~Model()
{

}

void Model::SaveResultFunction(double currentTime, const MatrixXi& currentState)
{
	while (nextSampleTime < currentTime)
	{
		solution.col(nextSampleTimeIndex) = Map<const VectorXi>(currentState.data(), currentState.cols()*currentState.rows());

		// cout << "Time Step " << fixed << setw(6) << nextSampleTimeIndex << ", " << setw(6) << setprecision(2) << 100.0 * currentTime / endTime << "% done" << endl;
		nextSampleTimeIndex++;
		if (nextSampleTimeIndex != sampleTimePointCount)
		{
			nextSampleTime = sampleTimePoints(nextSampleTimeIndex);
		}
		else
		{
			nextSampleTime = numeric_limits<double>::infinity();
		}
	}
}
