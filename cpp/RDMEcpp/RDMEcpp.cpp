#include <args/args.hxx>
#ifdef RDMEcppVisualStudioTest
#include "RDMEcppModelTest.hpp"
#else
#include <RDMEcppModel.hpp>
#endif
#include "NextSubvolumeMethod.h"

using namespace std;
int main(int argc, char* argv[])
{
	args::ArgumentParser parser("RDMEcpp version 1.0. A reaction-diffusion master equation simulator in C++.", "Copyright (C) 2016 Lukas Widmer. See http://www.csb.ethz.ch/tools/software/rdmecpp.html\nfor updates and more information.");
	args::HelpFlag help(parser, "help", "Display this help menu", { 'h', "help" });
	// args::Flag pgoFlag(parser, "pgo", "Do test run for profile-guided optimization.", { 'p', "pgo" });
	args::Group solvers(parser, "Solvers:", args::Group::Validators::AtMostOne);
	args::Flag nsmFlag(solvers, "nsm", "Use Next Subvolume Method (NSM) solver (default).", { 'n', "nsm" });
	args::Group requiredArguments(parser, "Required arguments:", args::Group::Validators::All);
	args::Positional<string> inputFile(requiredArguments, "inputFile", "Input .mat file");
	args::Positional<string> outputFile(requiredArguments, "outputFile", "Output .mat file");
	try
	{
		parser.ParseCLI(argc, argv);
	}
	catch (args::Help)
	{
		std::cout << parser;
		return 0;
	}
	catch (args::ParseError e)
	{
		std::cerr << e.what() << std::endl << std::endl;
		std::cerr << parser;
		return 1;
	}
	catch (args::ValidationError e)
	{
		if (!requiredArguments.Matched())
		{
			std::cerr << "Input / Output file must be specificed!" << std::endl << std::endl;
		}
		else
		{
			std::cerr << e.what() << std::endl << std::endl;
		}
		std::cerr << parser;
		return 1;
	}
	try
	{ 
		auto testModel = RDMEcppModel(args::get(inputFile));
		// BuddingYeastModel testModel("C:/Users/luwidmer/Documents/PhD/TubeX/URDME/urdme-master/examples/buddingYeast/umodLW.mat");

		cout << "Model loaded" << endl;

		cout << "Initializing NSM" << endl;
		NextSubvolumeMethod nsm(testModel);
		// testModel.SetUpdateFunction(bind(&NextSubvolumeMethod::UpdateSubvolumePropensities, &nsm, placeholders::_1));
		cout << "Initialization Done" << endl;

		nsm.Run();
	
		// auto file = MatlabFile::Create("C:/Users/luwidmer/Documents/PhD/TubeX/RDMEcpp/examples/mind_coli/coli_simulation_output.mat", "umod");
		auto file = MatlabFile::Create(args::get(outputFile), "umod");
		testModel.Save(file);
	}
	catch (std::exception& e)
	{
		cout << "Error: Exception occurred" << endl;
		cout << e.what() << endl;
		cin.get();
	}
	return 0;
}
