#pragma once
#include <vector>
#include <tuple>
#include <stddef.h>

/*
Data Layout: {0, 1, 2, 3, 4, 5, 6}

      0
     / \
	1   2
   /|   |\
  3 4   5 6
*/
namespace TournamentTree
{
	class TournamentTreePointer
	{
	protected:
		unsigned int nextPowerOfTwoMinusOne(unsigned int n)
		{
			n--;
			n |= (n >> 1);
			n |= (n >> 2);
			n |= (n >> 4);
			n |= (n >> 8);
			n |= (n >> 16);
			return n;
		}

	public:
		std::vector<double> data; // Save data as propensity and subvolume indices
		std::vector<double*> tournamentTree;
		size_t firstLastLevelIndex;
	
		TournamentTreePointer()
		{

		}
		TournamentTreePointer(const std::vector<double>& data)
		{
			this->data = data;

			auto tournamentTreeSize = nextPowerOfTwoMinusOne(data.size());
			// std::cout << "Tournament Tree size: " << tournamentTreeSize << std::endl;
			this->tournamentTree = std::vector<double*>(tournamentTreeSize);

			auto lastLastLevelIndex = (tournamentTreeSize >> 1) + (data.size() >> 1);
			firstLastLevelIndex = (tournamentTreeSize >> 1);
			// std::cout << "Level Above Offset: " << (tournamentTreeSize >> 1) << std::endl;
			// std::cout << "Data Size / 2: " << (data.size() >> 1) << std::endl;

			// Build tournament tree from leaves
			for (auto i = firstLastLevelIndex; i > 0; i--)
			{
				auto child1 = i << 1;
				auto child2 = child1 + 1;
				auto index = firstLastLevelIndex + i;
				
				if (child1 >= data.size())
				{
					tournamentTree[index] = NULL;
				}
				else if (child2 >= data.size() || data[child1] < data[child2])
				{
					tournamentTree[index] = &this->data[child1];
					// std::cout << "Child1: " << child1 << ", recovered: " << ((&this->data[child1]) - this->data.data())  << std::endl;
				}
				else
				{
					tournamentTree[index] = &this->data[child2];
					//std::cout << "Child2: " << child2 << ", recovered: " << ((&this->data[child2]) - this->data.data())  << std::endl;
				}

				
				// std::cout << "Index: " << index << ", child 1: " << child1 << ", child2: " << child2 << ", assigned leaf: " << tournamentTree[index] << std::endl;
			}
			if (data[0] < data[1])
			{
				tournamentTree[firstLastLevelIndex] = &this->data[0];
			}
			else
			{
				tournamentTree[firstLastLevelIndex] = &this->data[1];
			}
			// std::cout << "Index: " << firstLastLevelIndex << ", child 1: " << 0 << ", child2: " << 1 << std::endl;
			// Build tournament tree interior



			for (auto i = firstLastLevelIndex - 1 ; i > 0; i--)
			{
				auto child1 = 1 + (i << 1);
				auto child2 = child1 + 1;
				
				if (child1 >= data.size())
				{
					tournamentTree[i] = NULL;
				}
				else if (child2 >= data.size() || tournamentTree[child2] == NULL)
				{
					tournamentTree[i] = tournamentTree[child1];
				} 
				else if (tournamentTree[child1] == NULL)
				{
					tournamentTree[i] = tournamentTree[child2];
				}
				else if (*tournamentTree[child1] < *tournamentTree[child2])
				{
					tournamentTree[i] = tournamentTree[child1];
				}
				else
				{
					tournamentTree[i] = tournamentTree[child2];
				}
				// std::cout << "Index: " << i << ", child 1: " << child1 << ", child2: " << child2 << ", assigned leaf: " << tournamentTree[i] << std::endl;

			}

			if(*tournamentTree[1] < *tournamentTree[2])
			{
				tournamentTree[0] = tournamentTree[1];
			}
			else
			{
				tournamentTree[0] = tournamentTree[2];
			}
			/* for (decltype(tournamentTreeSize) i = 0; i < tournamentTreeSize; i++)
			{
				// std::cout << "Element: " << i << ", smallest index: " << tournamentTree[i] << std::endl;
			} */
		}

		std::tuple<size_t, double> GetMinimum()
		{
			return std::tuple<size_t, double>(tournamentTree[0] - data.data(), *tournamentTree[0]);
		}

		void UpdateLeaf(size_t leafIndex, double value)
		{
			auto leafPointer = &data[leafIndex];

			if (tournamentTree[0] == leafPointer && value < *leafPointer)
			{
				*leafPointer = value;
				return;
			}
			else
			{
				*leafPointer = value;
			}
			
			// Propagate new value up
			auto parent = firstLastLevelIndex + (leafIndex >> 1);

			// std::cout << "Updating element " << leafIndex << ", parent: " << parent << std::endl;

			size_t otherLeafIndex = leafIndex ^ 1;
			double* otherLeafPointer = &data[otherLeafIndex];
			double* minLeafPointer = leafPointer;
			double minLeafValue = value;

			if (otherLeafIndex < data.size())
			{
				double otherLeafValue = *otherLeafPointer;

				if (otherLeafValue <= value)
				{
					if (tournamentTree[parent] == otherLeafPointer)
						return;

					minLeafPointer = otherLeafPointer;
					minLeafValue = otherLeafValue;
				}
			}

			tournamentTree[parent] = minLeafPointer;
			UpdateTowardsRoot(parent, minLeafPointer, minLeafValue);
		}
			
		void UpdateTowardsRoot(size_t currentIndex, double* minLeafPointer, double minLeafValue)
		{
			auto parent = currentIndex;
			parent = (parent - 1) >> 1;
			size_t otherIndex;
			double* otherLeafPointer;
			double otherValue;

			do
			{
				// 253 -> 254
				// 254 -> 253
				// otherIndex = currentIndex + (currentIndex & 1) - !(currentIndex & 1);
				if (currentIndex % 2)
				{
					otherIndex = currentIndex + 1;
				}
				else
				{
					
					otherIndex = currentIndex - 1;
				}

				otherLeafPointer = tournamentTree[otherIndex];

				if (otherLeafPointer != NULL)
				{
					otherValue = *otherLeafPointer;

					if (otherValue <= minLeafValue)
					{
						if (tournamentTree[parent] == otherLeafPointer)
							return;

						minLeafPointer = otherLeafPointer;
						minLeafValue = otherValue;
					}
				}

				tournamentTree[parent] = minLeafPointer;

				currentIndex = parent;
				parent = (parent - 1) >> 1;
			} while (currentIndex > 0);
		}
	};
}