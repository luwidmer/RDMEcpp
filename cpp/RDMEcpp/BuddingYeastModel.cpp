#include "BuddingYeastModel.h"
#include <iostream>
#include <iomanip>

//
BuddingYeastModel::BuddingYeastModel(const MatlabFile& file) : Model(file)
{
	mtNodes = file.GetVector("mtNodes").cast<int>();
	tubulinsPerNode = file.GetValue("tubulinsPerNode");
	currentMtLength = file.GetValue("currentMtLength");
	maxMtLength = file.GetValue("maxMtLength");
	mtState = file.GetVector("mtState").cast<int>();

	mtEvents.resize(1);
	mtEventTimes.resize(1);

	mtSolution.resize(maxMtLength, sampleTimePointCount);

	propensityFunctions.reserve(4);
	propensityFunctions.push_back(std::bind(&BuddingYeastModel::rFun1, this, placeholders::_1, placeholders::_2));
	propensityFunctions.push_back(std::bind(&BuddingYeastModel::rFun2, this, placeholders::_1, placeholders::_2));
	propensityFunctions.push_back(std::bind(&BuddingYeastModel::rFun3, this, placeholders::_1, placeholders::_2));
	propensityFunctions.push_back(std::bind(&BuddingYeastModel::rFun4, this, placeholders::_1, placeholders::_2));

	reactionFunctions.reserve(4);
	reactionFunctions.push_back(std::bind(&BuddingYeastModel::executeR1, this, placeholders::_1, placeholders::_2, placeholders::_3));
	reactionFunctions.push_back(std::bind(&BuddingYeastModel::executeR2, this, placeholders::_1, placeholders::_2, placeholders::_3));
	reactionFunctions.push_back(std::bind(&BuddingYeastModel::executeR3, this, placeholders::_1, placeholders::_2, placeholders::_3));
	reactionFunctions.push_back(std::bind(&BuddingYeastModel::executeR4, this, placeholders::_1, placeholders::_2, placeholders::_3));
}

void BuddingYeastModel::Save(MatlabFile& file) 
{
	Model::Save(file);

	file.SetVector("mtNodes", mtNodes.cast<double>());
	file.SetValue("tubulinsPerNode", tubulinsPerNode);
	file.SetValue("currentMtLength", currentMtLength);
	file.SetValue("maxMtLength", maxMtLength);
	file.SetMatrix("mtSolution", mtSolution.cast<double>());


	mtEvents.conservativeResize(nMtEvents);
	mtEventTimes.conservativeResize(nMtEvents);
	file.SetVector("mtEvents", mtEvents.cast<double>());
	file.SetVector("mtEventTimes", mtEventTimes);
}


int BuddingYeastModel::GetCurrentNode()
{
	int currentNodeNumber = currentMtLength / tubulinsPerNode;
	if (currentNodeNumber >= maxMtLength / tubulinsPerNode)
	{
		throw runtime_error("Microtubule too long (last subVolume must not be completely filled with MT!).");
	}

	return mtNodes[currentNodeNumber];
}

bool BuddingYeastModel::CheckStateForConsistency(ConstState species, const long long int subVolumeIndex)
{
	if (currentMtLength < 0)
	{
		throw runtime_error("Microtubule too short (length < 0)!");
	}
	if (currentMtLength >= maxMtLength)
	{
		throw runtime_error("Microtubule too long (last subVolume must not be completely filled with MT!).");
	}

	int currentIndex = -1;
	(mtNodes.array() == subVolumeIndex).maxCoeff(&currentIndex);

	Eigen::VectorXi moleculeCount(nSpecies);
	moleculeCount.setZero();

	int currentMTIndex = currentIndex * tubulinsPerNode;
	int currentMTEndIndex = min((currentIndex + 1) * tubulinsPerNode, currentMtLength);

	for (; currentMTIndex < currentMTEndIndex; currentMTIndex++)
	{
		if (mtState[currentMTIndex] < 0)
		{
			break;
		}

		moleculeCount[mtState[currentMTIndex]]++;
	}

	if (false)
	{
	cout << "Species:" << endl << species << endl;
	cout << "Molecule Count on MT:" << endl << moleculeCount << endl;
	cout << "MT Length:" << currentMtLength << endl;
	
	// cout << "MtState:" << endl << mtState.head(10) << endl;

	for (int i = 0; i < currentMtLength; i++)
	{
		switch (mtState[i])
		{
			case -1:
				cout << "Shit -1!" << endl;
				throw runtime_error("Shit -1!");
				break;
			case 2:
				cout << "+";
				break;
			case 3:
				cout << "-";
				break;
			default:
				cout << "Shit ?!" << endl;
				throw runtime_error("Shit ?!");
				break;
		}
	}
	cout << endl;
	}

	if (((species - moleculeCount).segment(2,2).array() != 0).any())
	{
		cout << "Current Index: " << GetCurrentNode() << ", Index called: " << subVolumeIndex << endl;
		throw runtime_error("Current microtubule subvolume has species number discrepancy!");
	}
}

void BuddingYeastModel::SaveResultFunction(double currentTime, MatrixXi& currentState)
{
	// TODO: Clean this up...
	while (nextSampleTime < currentTime)
	{
		solution.col(nextSampleTimeIndex) = Map<VectorXi>(currentState.data(), currentState.cols()*currentState.rows());
		mtSolution.col(nextSampleTimeIndex) = mtState;

		cout << "Time Step " << fixed << setw(6) << nextSampleTimeIndex << ", " << setw(6) << setprecision(2) << 100.0 * currentTime / endTime << "% done" << endl;
		nextSampleTimeIndex++;
		if (nextSampleTimeIndex != sampleTimePointCount)
		{
			nextSampleTime = sampleTimePoints(nextSampleTimeIndex);
		}
		else
		{
			nextSampleTime = numeric_limits<double>::infinity();
		}
	}
}