#pragma once
#include "Model.h"
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_int_distribution.hpp>
#include <iostream>

class BuddingYeastModel : public Model
{
public:
	BuddingYeastModel(const MatlabFile& file);
	BuddingYeastModel(const string& path) : BuddingYeastModel(MatlabFile::Open(path, "umod")) {}

	virtual void Save(MatlabFile& file);

	Eigen::MatrixXi mtSolution;

	VectorXi mtEvents;
	VectorXd mtEventTimes;
	int nMtEvents = 0;

	Eigen::VectorXi mtNodes;
	Eigen::VectorXi mtState;
	int tubulinsPerNode;

	int currentMtLength;
	int maxMtLength;

	int GetCurrentNode();

	function<void(long long int)> updateNodeFunction;

	boost::random::mt11213b rng;

	const int cGDPTubulin = 0;
	const int cGTPTubulin = 1;
	const int cGTPTubulin_MT = 2;
	const int cGDPTubulin_MT = 3;

	const int CYTOSOL = 1;
	const int MICROTUBULE = 2;

	const double NA = 6.022e23;

	const double k_energy = 0; // 1/s
	const double k_attach = 3.2; // 1/(muM * s)
	const double k_hydrolysis = 0.12; // 1/s
	const double k_catastrophe = 290; // 1/s

	const double height = 2.92e-6; // Height in m for the 2d simulation - 3d would just use the volume of the element

	virtual void SaveResultFunction(double currentTime, MatrixXi&);

	bool CheckStateForConsistency(ConstState species, const long long int subVolumeIndex);

	void SetUpdateFunction(const function<void(long long int)> updateNodeFunction)
	{
		this->updateNodeFunction = updateNodeFunction;
	}

	void UpdateMTSolution(int eventNumber, double eventTime)
	{
		nMtEvents++;
		if (mtEvents.size() < nMtEvents)
		{
			// Double mtEvents size
			decltype(mtEvents.size()) doubleSize = 2 * mtEvents.size();

			mtEvents.conservativeResize(doubleSize);
			mtEvents[nMtEvents - 1] = eventNumber;

			mtEventTimes.conservativeResize(doubleSize);
			mtEventTimes[nMtEvents - 1] = eventTime;
		}
	}

	double rFun1(ConstState species, const long long int subVolumeIndex)
	{
		/* cGDPTubulin -> cGTPTubulin (Energy Reaction) */
		return k_energy*species[cGDPTubulin];
		//if (subDomains(subVolumeIndex) == CYTOSOL)
		//	
		//else
		//	return 0.0;
	}

	double rFun2(ConstState species, const long long int subVolumeIndex)
	{
		/* cGTPTubulin -> cGTPTubulin_MT (MT Growth) */
		/* if (subDomain == MICROTUBULE && TODO TIP CONDITION)
		return kdd*species[MinD_c_atp] * species[MinD_m] / (1000.0*NA*volume); */

		if (subVolumeIndex == GetCurrentNode())
		{
			auto nPerCubicMeter = species[cGTPTubulin] / (volumes(subVolumeIndex) * height);
			auto molPerCubicMeter = nPerCubicMeter / NA;
			auto microMolPerLiter = molPerCubicMeter * 1e6 * 1e-3;
			auto rate = k_attach * microMolPerLiter;
			return rate;
		}
		else
		{
			return 0.0;
		}
	}

	double rFun3(ConstState species, const long long int subVolumeIndex)
	{
		if (subDomains(subVolumeIndex) == MICROTUBULE)
		{
			return k_hydrolysis * species[cGTPTubulin_MT]; // Number of GTP tublins in subvolume
		}
		else
		{
			return 0.0;
		}
	}

	double rFun4(ConstState species, const long long int subVolumeIndex)
	{
		/* cGDPTubulin_MT -> cGDPTubulin (MT Shrinkage) */
		/*if (subDomain == MICROTUBULE && TODO TIP CONDITION)
		return ke*species[MinDE]; */
		//cout << "MtState:" << endl << mtState.head(10) << endl;
		if (currentMtLength > 0 && mtState[currentMtLength - 1] == cGDPTubulin_MT)
		{
			if (subVolumeIndex == GetCurrentNode() && (currentMtLength % tubulinsPerNode) > 0)
			{
				return k_catastrophe;
			}	
			else if ((currentMtLength % tubulinsPerNode) == 0 && subVolumeIndex == mtNodes[currentMtLength / tubulinsPerNode - 1])
			{
				return k_catastrophe;
			}
			else
			{
				return 0.0;
			}
		}
		else
		{
			return 0.0;
		}
	}

	void executeR1(double currentTime, State species, const long long int subVolumeIndex)
	{
		UpdateMTSolution(1, currentTime);
		/* cGDPTubulin -> cGTPTubulin (Energy Reaction) */
		// No MT action
	}

	void executeR2(double currentTime, State species, const long long int subVolumeIndex)
	{
		cout << "Execute R2" << endl;
		if (GetCurrentNode() != subVolumeIndex)
		{
			cout << "MT Subvolume: " << GetCurrentNode() << ", Solver Subvolume: " << subVolumeIndex << endl;
			throw runtime_error("Wrong MT Subvolume for attachment reaction!");
		}

		/* cGTPTubulin -> cGTPTubulin_MT (MT Growth) */

		// Update internal MT state: Add GTP Tubulin
		// species[cGTPTubulin]--; // Performed with stoichiometric matrix already
		// species[cGTPTubulin_MT]++; // Performed with stoichiometric matrix already
		mtState[currentMtLength] = cGTPTubulin_MT; // mtState[currentMtLength] is still empty as mtState is zero-based
		currentMtLength++;

		// Check MT state for consistency
		
		CheckStateForConsistency(species, subVolumeIndex);
		if ((currentMtLength % tubulinsPerNode) == 0)
		{
			// Update next node since it is now the active one
			// CheckStateForConsistency(species, mtNodes[currentMtLength / tubulinsPerNode]); 
			updateNodeFunction(mtNodes[currentMtLength / tubulinsPerNode]);
		}
		UpdateMTSolution(2, currentTime);
		// updateNodeFunction(subVolumeIndex);
	}

	void executeR3(double currentTime, State species, const long long int subVolumeIndex)
	{
		cout << "Execute R3" << endl;
		// Update internal MT state: Hydrolyze Random GTP Tubulin
		if (species[cGTPTubulin_MT] < 0)
		{
			cout << species << endl;
			cout << "Cannot execute hydrolyzation event without a GTP tubulin!" << endl;
			throw runtime_error("Cannot execute hydrolyzation event without a GTP tubulin!");
		}

		// In RDME Framework
		// species[cGTPTubulin_MT]--;
		// species[cGDPTubulin_MT]++;

		// In MT
		boost::random::uniform_int_distribution<> dist(1, species[cGTPTubulin_MT] + 1); // species array was already hydrolyzed
		auto tubulinToHydrolyze = dist(rng);

		int gtpTubulinNumber = 0;
		int currentIndex = -1;
		(mtNodes.array() == subVolumeIndex).maxCoeff(&currentIndex);

		int currentMTIndex = currentIndex * tubulinsPerNode;
		int currentMTEndIndex = min((currentIndex + 1) * tubulinsPerNode, currentMtLength);
		for (; currentMTIndex < currentMtLength; currentMTIndex++)
		{
			if (mtState[currentMTIndex] == cGTPTubulin_MT)
			{
				gtpTubulinNumber++;
			}
			if (gtpTubulinNumber == tubulinToHydrolyze)
			{
				break;
			}
		}

		mtState[currentMTIndex] = cGDPTubulin_MT;

		// Check MT state for consistency
		CheckStateForConsistency(species, subVolumeIndex);


		UpdateMTSolution(3, currentTime);
		// updateNodeFunction(subVolumeIndex);
	}

	void executeR4(double currentTime, State species, const long long int subVolumeIndex)
	{
		cout << "Execute R4" << endl;
		/* cGDPTubulin_MT -> cGDPTubulin (MT Shrinkage) */
		if (GetCurrentNode() != subVolumeIndex && (currentMtLength % tubulinsPerNode != 0))
		{
			cout << "MT Subvolume: " << GetCurrentNode() << ", Solver Subvolume: " << subVolumeIndex << endl;
			throw runtime_error("Wrong MT Subvolume for attachment reaction!");
		}


		// Update internal MT state: Remove GDP Tubulin
		// species[cGDPTubulin_MT]--;
		// species[cGDPTubulin]++;
		mtState[currentMtLength - 1] = -1;
		currentMtLength--;

		// Check MT state for consistency
		CheckStateForConsistency(species, subVolumeIndex);

		if ((currentMtLength % tubulinsPerNode) == 0 && currentMtLength > 0)
		{
			// Update previous node since it is now the active one
			updateNodeFunction(mtNodes[currentMtLength / tubulinsPerNode - 1]);
		}

		// updateNodeFunction(subVolumeIndex);
	}

};

