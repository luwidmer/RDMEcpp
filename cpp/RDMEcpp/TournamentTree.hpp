#pragma once
#include <vector>
#include <tuple>

/*
Data Layout: {0, 1, 2, 3, 4, 5, 6}

      0
     / \
	1   2
   /|   |\
  3 4   5 6
*/
namespace TournamentTree
{
	template <typename ValueType, typename IndexType>
	class TournamentTree
	{
	protected:
		unsigned int nextPowerOfTwoMinusOne(unsigned int n)
		{
			n--;
			n |= (n >> 1);
			n |= (n >> 2);
			n |= (n >> 4);
			n |= (n >> 8);
			n |= (n >> 16);
			return n;
		}

	public:
		std::vector<ValueType> data; // Save data as propensity and subvolume indices
		std::vector<IndexType> tournamentTree;
		IndexType firstLastLevelIndex;
	
		TournamentTree()
		{

		}
		TournamentTree(const std::vector<ValueType>& data)
		{
			this->data = data;

			auto tournamentTreeSize = nextPowerOfTwoMinusOne(data.size());
			// std::cout << "Tournament Tree size: " << tournamentTreeSize << std::endl;
			this->tournamentTree = std::vector<IndexType>(tournamentTreeSize);

			auto lastLastLevelIndex = (tournamentTreeSize >> 1) + (data.size() >> 1);
			firstLastLevelIndex = (tournamentTreeSize >> 1);
			// std::cout << "Level Above Offset: " << (tournamentTreeSize >> 1) << std::endl;
			// std::cout << "Data Size / 2: " << (data.size() >> 1) << std::endl;

			// Build tournament tree from leaves
			for (auto i = firstLastLevelIndex; i > 0; i--)
			{
				auto child1 = i << 1;
				auto child2 = child1 + 1;
				auto index = firstLastLevelIndex + i;
				
				if (child1 >= data.size())
				{
					tournamentTree[index] = -1;
				}
				else if (child2 >= data.size() || data[child1] < data[child2])
				{
					tournamentTree[index] = child1;
				}
				else
				{
					tournamentTree[index] = child2;
				}
				// std::cout << "Index: " << index << ", child 1: " << child1 << ", child2: " << child2 << ", assigned leaf: " << tournamentTree[index] << std::endl;
			}
			if (data[0] < data[1])
			{
				tournamentTree[firstLastLevelIndex] = 0;
			}
			else
			{
				tournamentTree[firstLastLevelIndex] = 1;
			}
			// std::cout << "Index: " << firstLastLevelIndex << ", child 1: " << 0 << ", child2: " << 1 << std::endl;
			// Build tournament tree interior



			for (auto i = firstLastLevelIndex - 1 ; i > 0; i--)
			{
				auto child1 = 1 + (i << 1);
				auto child2 = child1 + 1;
				
				if (child1 >= data.size())
				{
					tournamentTree[i] = -1;
				}
				else if (child2 >= data.size() || tournamentTree[child2] == -1)
				{
					tournamentTree[i] = tournamentTree[child1];
				} 
				else if (tournamentTree[child1] == -1)
				{
					tournamentTree[i] = tournamentTree[child2];
				}
				else if (data[tournamentTree[child1]] < data[tournamentTree[child2]])
				{
					tournamentTree[i] = tournamentTree[child1];
				}
				else
				{
					tournamentTree[i] = tournamentTree[child2];
				}
				// std::cout << "Index: " << i << ", child 1: " << child1 << ", child2: " << child2 << ", assigned leaf: " << tournamentTree[i] << std::endl;

			}

			if(data[tournamentTree[1]] < data[tournamentTree[2]])
			{
				tournamentTree[0] = tournamentTree[1];
			}
			else
			{
				tournamentTree[0] = tournamentTree[2];
			}
			/* for (decltype(tournamentTreeSize) i = 0; i < tournamentTreeSize; i++)
			{
				// std::cout << "Element: " << i << ", smallest index: " << tournamentTree[i] << std::endl;
			} */
		}

		std::tuple<IndexType, ValueType> GetMinimum()
		{
			return std::tuple<IndexType, ValueType>(tournamentTree[0], data[tournamentTree[0]]);
		}



		void UpdateLeaf(IndexType leafIndex, ValueType value)
		{
			if (tournamentTree[0] == leafIndex && value < data[leafIndex])
			{
				data[leafIndex] = value;
				return;
			}
			else
			{
				data[leafIndex] = value;
			}
			
			// Propagate new value up
			auto parent = firstLastLevelIndex + (leafIndex >> 1);

			// std::cout << "Updating element " << leafIndex << ", parent: " << parent << std::endl;

			IndexType otherLeafIndex = leafIndex ^ 1;
			IndexType minLeafIndex = leafIndex;
			ValueType minLeafValue = value;

			if (otherLeafIndex < data.size())
			{
				ValueType otherLeafValue = data[otherLeafIndex];

				if (otherLeafValue <= value)
				{
					if (tournamentTree[parent] == otherLeafIndex)
						return;

					minLeafIndex = otherLeafIndex;
					minLeafValue = otherLeafValue;
				}
			}

			tournamentTree[parent] = minLeafIndex;
			UpdateTowardsRoot(parent, minLeafIndex, minLeafValue);
		}
			
		void UpdateTowardsRoot(IndexType currentIndex, IndexType minLeafIndex, ValueType minLeafValue)
		{
			auto parent = currentIndex;
			parent = (parent - 1) >> 1;
			IndexType otherIndex;
			IndexType otherLeafIndex;
			ValueType otherValue;


			do
			{
				// 253 -> 254
				// 254 -> 253
				// otherIndex = currentIndex + (currentIndex & 1) - !(currentIndex & 1);
				if (currentIndex % 2)
				{
					otherIndex = currentIndex + 1;
				}
				else
				{
					
					otherIndex = currentIndex - 1;
				}

				otherLeafIndex = tournamentTree[otherIndex];

				if (otherLeafIndex >= 0)
				{
					otherValue = data[otherLeafIndex];

					if (otherValue <= minLeafValue)
					{
						if (tournamentTree[parent] == otherLeafIndex)
						{
							return;
						}

						minLeafIndex = otherLeafIndex;
						minLeafValue = otherValue;
					}
				}

				tournamentTree[parent] = minLeafIndex;

				currentIndex = parent;
				parent = (parent - 1) >> 1;
			} while (currentIndex > 0);
		}
	};
}