#pragma once
#include <string>
#include <Eigen/Dense>
#include <Eigen/SparseCore>
#include <matFileCpp/MatlabFile.hpp>
#include <functional>
#include <vector>

using namespace std;
using namespace Eigen;

typedef const DenseBase<MatrixXi>::ColXpr & State;
typedef const DenseBase<MatrixXi>::ColXpr & ConstState;

class Model
{
public:
	Model(const MatlabFile& file);
	Model(const string& path) : Model(MatlabFile::Open(path, "umod")) {};

	// void Save(const string& path);
	virtual void Save(MatlabFile& file);

	virtual ~Model();

	// vector<function<double(VectorType, VectorType, const double&, const int&)>> propensityFunctions;  // Propensity Functions
	// vector<function<double(VectorType, VectorType, const double, const int, const int)>> propensityFunctions;  // Propensity Functions
	vector<function<double(ConstState, const long long int)>> propensityFunctions;  // Propensity Functions
	vector<function<void(double, State, const long long int)>> reactionFunctions; // Reaction Functions (execute reaction)

	VectorXd sampleTimePoints;	// Vector of time points at which to save samples, rows: time points

	VectorXi subDomains;	    // Subdomain vector, rows: subdomain of each subvolume
	
	MatrixXd customData;		// Custom data matrix (custom # values specified for each subvolume), rows: subvolumes, columns: custom data points for each subvolume
	MatrixXi solution;			// rows: degrees of freedom, columns: sampling points (solution.col(i) corresponds to sampleTimePoints(i))
	MatrixXi intialCondition;	// rows: species, columns: subvolumes, entries: concentrations

	bool hasSeed;	// True if random seed was specified
	double seed;	// Random seed value

	SparseMatrix<int> stoichiometricMatrix;     // rows: species, columns: reactions
	SparseMatrix<int> dependencyGraphDiffusion; // rows: propensities to updated, columns: correspond to diffusive event of a certain species
	SparseMatrix<int> dependencyGraphReaction;  // rows: propensities to updated, columns: correspond to certain reaction
	SparseMatrix<double> diffusionMatrix;          // Diffusion Matrix: degrees of freedom x degrees of freedom matrix

	VectorXd volumes; // rows: volumes for 

	long long int nCells;
	long long int nReactions;
	long long int nSpecies;

	
	double nextSampleTime = -1;
	long long int nextSampleTimeIndex = 0;
	long long int sampleTimePointCount = -1; 
	double endTime = -1;

	virtual void SaveResultFunction(double currentTime, const MatrixXi&);

	// bool writeReport;
};

