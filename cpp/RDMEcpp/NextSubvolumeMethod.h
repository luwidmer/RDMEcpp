#pragma once
#include "Model.h"
#include "TournamentTree.hpp"
#include "TournamentTreePointer.hpp"
#include <boost/random/taus88.hpp>
#include <boost/random/uniform_01.hpp>
#include <boost/random/exponential_distribution.hpp>

#include "BinHeap.hpp"
class NextSubvolumeMethod
{
public:
	NextSubvolumeMethod(Model &model);
	~NextSubvolumeMethod();

	void Run();
	void UpdateSubvolumePropensities(int subVolumeIndex);
protected:
	Model &model;

	Eigen::MatrixXd subvolumeReactionRates;
	Eigen::VectorXd totalSubvolumeReactionRates;
	Eigen::VectorXd totalSubvolumeDiffusionRates;
	Eigen::VectorXd totalSubvolumeRates;
	Eigen::VectorXd diffusionMatrixDiagonal;
	Eigen::MatrixXi currentState;

	double currentTime = model.sampleTimePoints(0);
	// BinHeap<int, double> reactionRateTimes;
	// TournamentTree::TournamentTree<double, short> tournamentTree;
	TournamentTree::TournamentTreePointer tournamentTreePointer;
	// gheap<2, 32> fastHeap;

	
	boost::random::taus88 unifRng;
	boost::random::uniform_01<double> unifDist;
	// boost::random::mt11213b expRng;
	boost::random::exponential_distribution<double> expDist;
	
	void PerformReactionEvent(double randEvent, int nextReactionVolumeIndex);
	int PerformDiffusionEvent(double randEvent, int nextReactionVolumeIndex);
	
	long long int totalReactionEvents;
	long long int totalDiffusionEvents;
	
};

