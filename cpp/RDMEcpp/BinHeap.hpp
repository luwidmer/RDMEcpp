#pragma once
#include <vector>
#include <tuple>

/*
Data Layout: {0, 1, 2, 3, 4, 5, 6}

      0
     / \
	1   2
   /|   |\
  3 4   5 6
*/
template <typename BinHeapIndexType, typename BinHeapValueType>
class BinHeap
{
public:
	std::vector<std::tuple<BinHeapValueType, BinHeapIndexType>> data_; // Save data as propensity and subvolume indices
	std::vector<BinHeapIndexType> positionInHeap_; // Gives the position of the <reactionRate, subvolumeIndex> tuple in the binheap (required for updating a specific node in diffusion events)
	BinHeapIndexType dataSize = 0;
	BinHeapIndexType dataSizeMinus1 = -1;
	BinHeap()
	{

	}
	BinHeap(const std::vector<std::tuple<BinHeapValueType, BinHeapIndexType>>& data)
	{
		data_ = data;
		dataSize = data.size();
		dataSizeMinus1 = dataSize - 1;

		std::sort(data_.begin(), data_.end(), [](std::tuple<BinHeapValueType, BinHeapIndexType> a, std::tuple<BinHeapValueType, BinHeapIndexType> b) {
			return std::get<0>(a) < std::get<0>(b);
		});
		
		positionInHeap_ = std::vector<BinHeapIndexType>(dataSize);
		
		for (BinHeapIndexType i = 0; i < dataSize; i++)
		{
			positionInHeap_[std::get<1>(data_[i])] = i; 
		}

#ifdef _DEBUG
		VerifyHeapProperty();
		VerifyMinProperty();
#endif
	}
	
	void Update(BinHeapIndexType binHeapIndex)
	{
		int parent = (binHeapIndex - 1) >> 1;

		if (binHeapIndex == 0 || std::get<0>(data_[binHeapIndex]) >= std::get<0>(data_[parent]))
		{
			PercolateNodeDown(binHeapIndex);
			
		}
		else
		{
			PercolateNodeUp(binHeapIndex);
		}

#ifdef _DEBUG
		VerifyHeapProperty();
		VerifyMinProperty();
#endif
	}

	void PercolateNodeDown(BinHeapIndexType binHeapIndex)
	{
		auto keyAndIndex = data_[binHeapIndex];

		auto currentBinHeapIndex = binHeapIndex;
		decltype(binHeapIndex) child;
		while ((child = (currentBinHeapIndex << 1) + 1) < dataSize)
		{
			if (child != dataSizeMinus1 && std::get<0>(data_[child + 1]) < std::get<0>(data_[child]))
			{
				child++;
			}

			if (std::get<0>(data_[child]) < std::get<0>(keyAndIndex))
			{
				positionInHeap_[std::get<1>(data_[child])] = currentBinHeapIndex;
				data_[currentBinHeapIndex] = data_[child];
			}
			else
			{
				break;
			}

			currentBinHeapIndex = child;
		}

		data_[currentBinHeapIndex] = keyAndIndex;
		positionInHeap_[std::get<1>(keyAndIndex)] = currentBinHeapIndex;
	}

	void PercolateNodeUp(BinHeapIndexType binHeapIndex)
	{
		auto keyAndIndex = data_[binHeapIndex];
		auto currentBinHeapIndex = binHeapIndex;

		decltype(binHeapIndex) parent;
		do
		{
			parent = (currentBinHeapIndex - 1) >> 1;
			if (std::get<0>(keyAndIndex) < std::get<0>(data_[parent]))
			{
				// Swap nodes
				positionInHeap_[std::get<1>(data_[parent])] = currentBinHeapIndex;
				data_[currentBinHeapIndex] = data_[parent];
			}
			else
			{
				break;
			}
			// Traverse upwards
			currentBinHeapIndex = parent;
		} while (parent > 0);

		data_[currentBinHeapIndex] = keyAndIndex;
		positionInHeap_[std::get<1>(keyAndIndex)] = currentBinHeapIndex;
	}

	void VerifyHeapProperty()
	{
		for (BinHeapIndexType i = 0; i < dataSizeMinus1 / 2; i++)
		{
			if (data_[i] > data_[2 * i + 1] || data_[i] > data_[2 * i + 2])
			{
				throw std::runtime_error("Binheap failure, exiting!");
			}
		}
	}

	void VerifyMinProperty()
	{
		double minimum = std::get<0>(data_[0]);

		for (BinHeapIndexType i = 1; i < dataSize; i++)
		{
			if (std::get<0>(data_[i]) < minimum)
			{
				throw std::runtime_error("Binheap failure, exiting!");
			}
		}
	}
};
