#pragma once
#include "../../cpp/RDMEcpp/Model.h"


class RDMEcppModel : public Model
{
public:
	const int MinD_c_atp = 0;
	const int MinD_m = 1;
	const int MinD_e = 2;
	const int MinDE = 3;
	const int MinD_c_adp = 4;

	const int CYTOSOL = 1;
	const int MEMBRANE = 2;

	/* Rate constants. */
	const double NA = 6.022e23;
	const double kd = 1.25e-8;
	const double kdd = 9.0e6;
	const double kde = 5.56e7;
	const double ke = 0.7;
	const double k_adp = 1.0;

	double rFun1(ConstState species, const long long int subVolumeIndex)
	{	
		/* MinD_c_atp -> MinD_m */
		if (subDomains(subVolumeIndex) == MEMBRANE)
			return kd*species[MinD_c_atp] / customData(0, subVolumeIndex);
		return 0.0;
	}

	double rFun2(ConstState species, const long long int subVolumeIndex)
	{
		/* MinD_c_atp + MinD_m -> 2MinD_m */
		if (subDomains(subVolumeIndex) == MEMBRANE)
			return kdd*species[MinD_c_atp] * species[MinD_m] / (1000.0*NA*volumes(subVolumeIndex));
		return 0.0;
	}

	double rFun3(ConstState species, const long long int subVolumeIndex)
	{
		/* MinD_m + MinD_e -> MinDE */
		if (subDomains(subVolumeIndex) == MEMBRANE)
			return kde*species[MinD_m] * species[MinD_e] / (1000.0*NA*volumes(subVolumeIndex));
		return 0.0;
	}

	double rFun4(ConstState species, const long long int subVolumeIndex)
	{
		/* MinDE -> MinD_e + MinD_c_adp*/
		if (subDomains(subVolumeIndex) == MEMBRANE)
			return ke*species[MinDE];
		return 0.0;
	}

	double rFun5(ConstState species, const long long int subVolumeIndex)
	{
		/* MinD_c_adp -> MinD_c_atp */
		return k_adp*species[MinD_c_adp];
	}

	RDMEcppModel(const MatlabFile& file) :
		Model(file)
	{
		cout << "File Constructor" << endl;
		propensityFunctions.reserve(5);

		propensityFunctions.push_back(std::bind(&RDMEcppModel::rFun1, this, std::placeholders::_1, std::placeholders::_2));
		propensityFunctions.push_back(std::bind(&RDMEcppModel::rFun2, this, std::placeholders::_1, std::placeholders::_2));
		propensityFunctions.push_back(std::bind(&RDMEcppModel::rFun3, this, std::placeholders::_1, std::placeholders::_2));
		propensityFunctions.push_back(std::bind(&RDMEcppModel::rFun4, this, std::placeholders::_1, std::placeholders::_2));
		propensityFunctions.push_back(std::bind(&RDMEcppModel::rFun5, this, std::placeholders::_1, std::placeholders::_2));
	}

	RDMEcppModel(const string& path) :
		RDMEcppModel(MatlabFile::Open(path, "umod"))
	{
		cout << "Path Constructor" << endl;
	}



};

