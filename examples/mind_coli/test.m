%% Clear workspace
clc;
clear all;
close all;
%% Load 
fem = mphload('coli_old.mph');
%%
umod = comsol2rdmecpp(fem, 3);
%%
umod.xmi = mphxmeshinfo(fem);

%%
% umod2 = comsol2rdmecpp(fem, 3);
% if ~all(umod.vol == umod2.vol)
%     warning('Nondeterministic behavior in COMSOL?!');
% end
%%
RDMEcpp_visualizeNodes(umod)
colormap winter

%%
umod = mind_coli(umod);
%%
RDMEcpp_validate(umod);

outDir = ['output-' datestr(clock, 30)];
mkdir(outDir);
inputFile = [outDir filesep 'coli_simulation_input.mat'];
RDMEcpp_save(umod, inputFile);


%%

RDMEcpp_compile;
%%
nRuns = 1;
elapsedTime = nan(nRuns,1);
for i=1:nRuns
    outputFile = [outDir filesep 'coli_simulation_output' datestr(clock, 30) '.mat'];
    tic
    RDMEcpp_run(inputFile,outputFile);
    elapsedTime(i) = toc
end
%%
umod = RDMEcpp_load(umod, outputFile);

%% Import results into COMSOL model
tic
umod = rdmecpp2comsol(umod,umod.U,umod.tspan,3);
toc
% Add a plot of the results
fem.result.create('res1','PlotGroup3D');
fem.result('res1').set('t','900');
fem.result('res1').feature.create('surf1', 'Surface');
fem.result('res1').feature('surf1').set('expr', 'MinD_m');
% Save the result
mphsave(fem, 'coli_result.mph');

%% Visualize in MATLAB and export to movie
%'colormap', brewermap(256,'*RdBu')
figure;
RDMEcpp_visualizeSurface(umod, 2, 'framerate', 30, 'movieFile', 'coli_video');
%%
RDMEcpp_openVideo('coli_video.avi');

%% Visualize only half
[ ~, nodes2dofs ] = RDMEcpp_nodeDofMapping( umod.xmi );
surfaceTriangles = nodes2dofs(1 + umod.xmi.elements.tri.nodes');
useTriangle = false([size(surfaceTriangles,1) 1]);

for i = 1:size(surfaceTriangles,1)
    triangleNodes = surfaceTriangles(i,:);
    z = umod.nodeCoords(3, triangleNodes);
    if all(z > 0)
        useTriangle(i) = true;
    end
end
surfaceTriangles = surfaceTriangles(useTriangle,:);
figure;
RDMEcpp_visualizeSurface(umod, 2, 'timeIndices', 181, 'surfaceTriangles', surfaceTriangles);
%%

