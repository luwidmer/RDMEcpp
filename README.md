Info
=====
RDMEcpp is a reaction-diffusion master equation simulation engine written in C++ [[1]].

License
-------
RDMEcpp is provided under the [Mozilla Public License Version 2.0](LICENSE).

Authors
-------

    Lukas Widmer <lukas.widmer@bsse.ethz.ch>


Installation
============

Windows
-------
1. Install MATLAB
2. Install Git via TortoiseGit (https://tortoisegit.org/)
3. Install Mercurial via TortoiseHg (https://tortoisehg.bitbucket.io/)
4. Install a C++ Compiler, e.g., Visual Studio (https://www.visualstudio.com/). If you do not run "Visual Studio 14 2015 Win64", adjust this setting in [Example/compileTest.m](Example/compileTest.m)
5. Install CMake (https://cmake.org/)
6. Install HDF5 binaries for your C++ compiler (https://support.hdfgroup.org/HDF5/)
7. Git clone this repo
8. Run `install_RDMEcpp.m` in MATLAB

Mac OS X
--------
1. Install MATLAB
2. Install Git through package manager: e.g., `brew install git`
3. Install Mercurial through package manager: e.g., `brew install mercurial`
4. Install a C++ Compiler, e.g., XCode (https://developer.apple.com/xcode/)
5. Install CMake (https://cmake.org/)
6. Install HDF5 through package manager: e.g., `brew install hdf5`
7. Git clone this repo
8. Run `install_RDMEcpp.m` in MATLAB

Linux (instructions here are for Ubuntu / Debian)
-------------------------------------------------
1. Install MATLAB
2. Install Git through package manager: `sudo apt-get install git`
3. Install Mercurial through package manager: `sudo apt-get install mercurial`
4. Install a C++ Compiler, e.g., g++: `sudo apt-get install g++`
5. Install CMake: `sudo apt-get install cmake`
6. Install HDF5: `apt-get install libhdf5-10 libhdf5-cpp-11 libhdf5-dev`
7. Git clone this repo
8. Run `install_RDMEcpp.m` in MATLAB

Cluster Usage
=============
Git and Mercurial should be installed by default.
1. Load MATLAB module: `module load matlab`
2. Load HDF5: `module load hdf5`
3. Load C++ compiler: `module load gcc/5.3.0`
4. Load CMake: `module load cmake/3.3.1`
5. Git clone this repo
6. Run `install_RDMEcpp.m` in MATLAB

Usage 
=====
See tutorial.

References
==========
[1]: http://google.com
1. Widmer LA, Stelling J (2017)  
RDMEcpp: a cross-platform, modular, and fast C++ reaction-diffusion master equation simulator  
[Submitted.][2]
[2]: https://gitlab.com/csb.ethz/matFileCpp
2. [matFileCpp Library][2]
[3]: http://eigen.tuxfamily.org
3. [Eigen Library][3]
[4]: https://support.hdfgroup.org/HDF5/
4. [HDF5 Library][4]