function install_RDMEcpp
    
    clc
    RDMEtoolboxRoot = fileparts(mfilename('fullpath'));
    currentFolder = cd([RDMEtoolboxRoot filesep 'matlab']);
    
    version = RDMEcpp_version();

    fprintf('\n');
    fprintf('---------------------------------------------------------------------------\n');
    fprintf('RDMEcpp Toolbox Version %s, http://www.csb.ethz.ch \n', version);
    fprintf('---------------------------------------------------------------------------\n');


    addPathToMATLAB = YNprompt('The default is to add the toolbox path to MATLAB, so you can use\nRDMEcpp from the command line in your current session.\nWould you like to add the path [Y/N]? ', true);

    if addPathToMATLAB
        addpath(pwd);
        addpath([RDMEtoolboxRoot filesep 'matlab' filesep 'comsolInterface']);

        printSeparator();

        savePaths = YNprompt('The default is to add the toolbox path to MATLAB permanently.\nWould you like to add the path permanently [Y/N]? ', true);

        if savePaths
            savepath;
        end
    end

    printSeparator();
    fprintf('RDMEcpp Toolbox Version %s installed successfully \n', version);
    cd(currentFolder);
    
    function answer = YNprompt(question, default)
        while true
            answer = input(question,'s');
            answer = upper(answer);

            if strcmp(answer, 'Y') || (isempty(answer) && default)
                answer = true;
                break;
            elseif strcmp(answer, 'N') || (isempty(answer) && ~default)
                answer = false;
                break;
            end
        end
    end

    function printSeparator()
        fprintf('\n');
        fprintf('---------------------------------------------------------------------------\n');
    end
end